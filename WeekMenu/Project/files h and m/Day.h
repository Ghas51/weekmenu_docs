#import <UIKit/UIKit.h>

@interface Day: NSObject

@property(nonatomic, strong) Recipe *breakfast;
@property(nonatomic, strong) Recipe *snack1;
@property(nonatomic, strong) Recipe *lunch;
@property(nonatomic, strong) Recipe *snack2;
@property(nonatomic, strong) Recipe *dinner;

@end