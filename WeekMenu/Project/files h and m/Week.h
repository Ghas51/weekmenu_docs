#import <UIKit/UIKit.h>

@interface Week: NSObject

@property(nonatomic, strong) NSArray<Day *> *daysArray;

@end