#import <UIKit/UIKit.h>
//Рецепт
@interface Recipe: NSObject

@property (nonatomic, strong)  NSString *nameRecipe;
@property (nonatomic, strong)  NSString *numberServings;
@property (nonatomic, strong)  NSString *cookingTime;
@property (nonatomic, strong)  NSString *nutritionalValue;
@property (nonatomic, strong)  NSString *products;
@property (nonatomic, strong)  NSString *cooking;
@property (nonatomic, strong)  UIImage *recipeImage;

@end