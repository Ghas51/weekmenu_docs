//
//  ViewController.m
//  WeekMenu_11.01.2000
//
//  Created by Ученик on 11.01.18.
//  Copyright © 2018 Luchin. All rights reserved.
//

#import "ViewController.h"
#import "TableViewCell.h"

@interface ViewController () <UITableViewDataSource>
@property (nonatomic, strong) NSArray *recipesArray;
@property (nonatomic, strong) IBOutlet UITableView *tableView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"identifier"];
    
    [self read];
}
- (void)read
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"DataBase" ofType:@"plist"];
    NSDictionary *rootDictionary = [NSMutableDictionary dictionaryWithContentsOfFile:filePath];
    
    self.recipesArray = [rootDictionary objectForKey:@"recipes"];
   // [self.textLabel setText:text];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _recipesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"identifier" forIndexPath:indexPath];
    
    NSInteger index = indexPath.row;
    NSDictionary *recipeDict = [_recipesArray objectAtIndex:index];
    NSString *name = [recipeDict objectForKey:@"name"];
    [cell.textLabel setText:name];
    
    return cell;
}

@end
