//
//  main.m
//  WeekMenu_11.01.2000
//
//  Created by Ученик on 11.01.18.
//  Copyright © 2018 Luchin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
