#import <UIKit/UIKit.h>
@class Day;
@interface Week: NSObject

@property(nonatomic, strong) NSArray<Day *> *daysArray;

@end