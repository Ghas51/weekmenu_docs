//
//  TableViewCell.h
//  WeekMenu_11.01.2000
//
//  Created by Ученик on 15.02.18.
//  Copyright © 2018 Luchin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *label;
@property (nonatomic, strong) IBOutlet UIImageView *image;
@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *servingsNumberLabel;
@property (nonatomic, strong) IBOutlet UILabel *cookingTimeLabel;
@property (nonatomic, strong) IBOutlet UILabel *caloriesLabel;
@property (nonatomic, strong) IBOutlet UILabel *proteinsLabel;
@property (nonatomic, strong) IBOutlet UILabel *fatsLabel;
@property (nonatomic, strong) IBOutlet UILabel *carbohydratesLabel;
@property (nonatomic, strong) IBOutlet UILabel *ingredientsLabel;
@property (nonatomic, strong) IBOutlet UILabel *cookingLabel;

@end
