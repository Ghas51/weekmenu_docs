//
//  ViewController.m
//  Lesson 02.11.17
//
//  Created by Ученик on 02.11.17.
//  Copyright © 2017 Luchin. All rights reserved.
//

#import "ViewController.h" //импортирование интерфейса класса

@interface ViewController () // объявлениеэлементов интерфейса
@property(nonatomic, weak) IBOutlet UIView *firstView;
@property(nonatomic, weak) IBOutlet UITextField *firstTextF;
@property(nonatomic, weak) IBOutlet UITextField *secondTextF;
@property(nonatomic, weak) IBOutlet UIButton *firstButton;
@property(nonatomic, weak) IBOutlet UILabel *firstLabel;

@end

@implementation ViewController 

- (void)viewDidLoad { // автоматически вызываемая функция
    [super viewDidLoad];
    
}

-(IBAction)buttonPressed:(id)sender //объявление функции
{
    float firstTF = self.firstTextF.text.floatValue; //получение значения из firstTextF
    float secondTF = self.secondTextF.text.floatValue; //получение значения из secondTextF
    float result = [self area:firstTF with:secondTF]; // записывание в переменную result ресультат работы функции
    [self print:result]; //вызов функции с передачей в функцию значение переменной result
    [self drawRectW:firstTF with:secondTF]; //вызов функции с передачей в функцию значения двух переменных
    
}

- (float)area:(float)width with:(float)height //объявление функции
{
    return width*height; //возвращение функцией результата: произведение 2-ух чисел типа float
}

- (void)print:(float)result //объявление функции, не возвращающей значений
{
    NSString *text = @(result).stringValue; // преобразование числа в строку и присваивание текстового значения переменной text
    [self.firstLabel setText:text]; // передача значения переменной text в firstLabel
}

- (void)drawRectW:(float)w with:(float)h //объявление функции, не возвращающей значений
{
    UIView *view = [UIView new];
    
    UIColor *color = [UIColor greenColor];
    [view setBackgroundColor:color]; //изменение цвета элемента UIView
    
    CGRect frame = CGRectMake(0, 0, w, h);
    [view setFrame:frame]; // передача параметров в UIView (начала координат, длина, ширина)
    
    [self.firstView addSubview:view]; 
}

@end
