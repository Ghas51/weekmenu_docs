//
//  ViewController.m
//  Lesson 02.11.17
//
//  Created by Ученик on 02.11.17.
//  Copyright © 2017 Luchin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property(nonatomic, weak) IBOutlet UIView *firstView;
@property(nonatomic, weak) IBOutlet UITextField *firstTextF;
@property(nonatomic, weak) IBOutlet UITextField *secondTextF;
@property(nonatomic, weak) IBOutlet UITextField *thirdTextF;
@property(nonatomic, weak) IBOutlet UITextField *fourthTextF;
@property(nonatomic, weak) IBOutlet UITextField *fifthTextF;
@property(nonatomic, weak) IBOutlet UIButton *firstButton;
@property(nonatomic, weak) IBOutlet UILabel *firstLabel;
@property(nonatomic, strong) UIView *secondView;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.secondView=[UIView new];
    [self.firstView addSubview:self.secondView];
    
}

-(IBAction)buttonPressed:(id)sender
{
    float firstTF = self.firstTextF.text.floatValue;
    float secondTF = self.secondTextF.text.floatValue;
    float thirdTF = self.thirdTextF.text.floatValue;
    float fourthTF = self.fourthTextF.text.floatValue;
    int fifthTF=self.fifthTextF.text.intValue;
    float result = [self area:firstTF with:secondTF];//ширина и высота
    [self print:result];
    CGRect frame=CGRectMake(thirdTF,fourthTF,firstTF,secondTF);
    UIColor *color;
    switch(fifthTF){
        case 1:{
            color = [UIColor greenColor];
            break;
        }
        case 2:{
            color = [UIColor brownColor];
            break;
        }
        case 3:{
            color = [UIColor blackColor];
            break;
        }
        case 4:{
            color = [UIColor yellowColor];
            break;
        }
        case 5:{
            color = [UIColor redColor];
            break;
        }
        default:
            color = [UIColor whiteColor];
            break;
    }
    
    
    [self drawRectW:frame with:color];
    
    
}

- (float)area:(float)width with:(float)height
{
    return width*height;
}

- (void)print:(float)result
{
    NSString *text = @(result).stringValue;
    [self.firstLabel setText:text];
}

- (void)drawRectW:(CGRect)frame with:(UIColor*)color
{
   
    [self.secondView setBackgroundColor:color];

    [self.secondView setFrame:frame];
    
    
}

@end
