#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, weak) IBOutlet UILabel *label;
@property (nonatomic, weak) IBOutlet UIView *view;

@end

@implementation ViewController

- (IBAction)button_pressed
{
    float width = self.view.frame.size.width;
    float height = self.view.frame.size.height;
    //Вызвать функцию вычисляющую периметр
    [self calculatePerimeter: width with: height];
}
//Обявить функцию, которая принимает два аргумента, вычисляет периметр
- (float)calculatePerimeter:(float)a with:(float)b
{
    float c = 2*(a+b);
    [self printPerimeter:c]; //и вызывает функцию printPerimeter: для вывода значения в label
}

- (void)printPerimeter:(float)value
{
    NSString *text = @(value).stringValue;
    [self.label setText:text];
}

@end
