//
//  UserDefaultsOne.m
//  StoreData tutorial
//
//  Created by Andrey Vasilev on 25.01.2018.
//  Copyright © 2018 Andrey Vasilev. All rights reserved.
//

#import "UserDefaultsOne.h"

@interface UserDefaultsOne ()

@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UITextField *textField2;
@property (nonatomic, strong) UILabel *textLabel;
@property (nonatomic, strong) UIButton *writeButton;
@property (nonatomic, strong) UIButton *readButton;

@end

@implementation UserDefaultsOne

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    float keyWindowHeight = [UIApplication sharedApplication].keyWindow.frame.size.width;
    
    
    self.textField2 = [[UITextField alloc] initWithFrame:CGRectMake(20, 0, keyWindowHeight-40, 40)];
    [self.textField2 setBorderStyle:UITextBorderStyleLine];
    [self.view addSubview:self.textField2];
    
    self.textField = [[UITextField alloc] initWithFrame:CGRectMake(20, 40, keyWindowHeight-40, 40)];
    [self.textField setBorderStyle:UITextBorderStyleLine];
    [self.view addSubview:self.textField];
    
    self.writeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.writeButton.frame = CGRectMake(20, 80,keyWindowHeight-40, 40);
    [self.writeButton setTitle:@"Сохранить" forState:UIControlStateNormal];
    [self.writeButton addTarget:self action:@selector(write) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.writeButton];
    
    self.readButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.readButton.frame = CGRectMake(20, 120,keyWindowHeight-40, 40);
    [self.readButton setTitle:@"Прочитать" forState:UIControlStateNormal];
    [self.readButton addTarget:self action:@selector(read) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.readButton];
    
    self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 160, keyWindowHeight-40, 40)];
    [self.view addSubview:self.textLabel];
}

- (void)write
{
    NSString *key = self.textField2.text;
    NSString *value = self.textField.text;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:value forKey:key];
    [userDefaults synchronize];
}

- (void)read
{
    NSString *key = self.textField2.text;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *text = [userDefaults objectForKey:key];
    [self.textLabel setText:text];
}

@end
