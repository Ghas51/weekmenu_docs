//
//  main.m
//  Store Data
//
//  Created by Учитель on 25.01.18.
//  Copyright (c) 2018 Учитель. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
