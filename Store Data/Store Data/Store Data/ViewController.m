//
//  ViewController.m
//  StoreData tutorial
//
//  Created by Andrey Vasilev on 25.01.2018.
//  Copyright © 2018 Andrey Vasilev. All rights reserved.
//

#import "ViewController.h"
#import "UserDefaultsOne.h"
#import "UserDefaultsMany.h"
#import "PlistOneViewController.h"

static NSString * const kReuseIdentifier = @"identifier";

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureTableView];
}

- (void)configureTableView
{
    _tableView = [[UITableView alloc] init];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kReuseIdentifier];
    [self.view addSubview:_tableView];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    _tableView.frame = self.view.bounds;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kReuseIdentifier forIndexPath:indexPath];
    switch (indexPath.row) {
        case 0:
            [cell.textLabel setText:@"NSUserDefaults - Один объект"];
            break;
        case 1:
            [cell.textLabel setText:@"NSUserDefaults - Много объектов"];
            break;
        case 2:
            [cell.textLabel setText:@"Property List.plist"];
            break;
        default:
            break;
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *viewController;
    switch (indexPath.row) {
        case 0:
            viewController = [UserDefaultsOne new];
            break;
        case 1:
            viewController = [UserDefaultsMany new];
            break;
        case 2:
            viewController = [PlistOneViewController new];
            break;
        default:
            viewController = [UIViewController new];
            break;
    }
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
