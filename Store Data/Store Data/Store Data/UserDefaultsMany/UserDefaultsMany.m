//
//  UserDefaultsMany.m
//  StoreData tutorial
//
//  Created by Andrey Vasilev on 25.01.2018.
//  Copyright © 2018 Andrey Vasilev. All rights reserved.
//

#import "UserDefaultsMany.h"

static NSString * const kUserDefaultsManyReuseIdentifier = @"identifier";

@interface UserDefaultsMany () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation UserDefaultsMany

- (void)viewDidLoad {
    [super viewDidLoad];
    self.array = [[NSMutableArray alloc] init];
    
    [self configureTableView];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(write)];
    [self.navigationItem setRightBarButtonItem:item];
}

- (void)configureTableView
{
    _tableView = [[UITableView alloc] init];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kUserDefaultsManyReuseIdentifier];
    [self.view addSubview:_tableView];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    _tableView.frame = self.view.bounds;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self read];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kUserDefaultsManyReuseIdentifier forIndexPath:indexPath];
    NSString *text = self.array[indexPath.row];
    [cell.textLabel setText:text];
    return cell;
}

#pragma mark Read / Write

- (void)read
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *arr = [userDefaults objectForKey:@"array"];
    if (arr != nil) {
        self.array = [NSMutableArray arrayWithArray:arr];
    }
    
    [self.tableView reloadData];
}

- (void)write
{
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"d MMM yyyy HH:mm:ss"];
    NSString *text = [formatter stringFromDate:date];
    
    [self.array addObject:text];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:self.array forKey:@"array"];
    [userDefaults synchronize];
    
    [self read];
}


@end
